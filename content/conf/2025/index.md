---
title: PravConf 2025
date: 2025-02-23
---

<!-- for volunteers list -->
<style>
  .volunteers-list {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    gap: 20px;
    padding: 20px;
    max-width: 1200px;
    margin: 0 auto;
  }

  .volunteer-card {
    width: 220px;
    padding: 10px;
    border: 2px solid #ddd;
    border-radius: 8px;
    text-align: center;
    cursor: pointer;
    transition: transform 0.2s ease;
    background-color: #fff;
  }

  .volunteer-card:hover {
    transform: scale(1.05);
  }

  .volunteer-photo {
    width: 100%;
    height: 200px;
    object-fit: cover;
    border-radius: 50%;
  }

  .volunteer-name {
    font-size: 1.2em;
    margin-top: 10px;
  }

  .volunteer-title {
    font-size: 0.9em;
    color: gray;
  }

  @media (max-width: 768px) {
    .volunteers-list {
      justify-content: space-around;
      padding: 10px;
    }

    .volunteer-card {
      width: 150px;
    }

    .volunteer-photo {
      height: 150px;
    }
  }

  @media (max-width: 480px) {
    .volunteers-list {
      justify-content: center;
    }

    .volunteer-card {
      width: 120px;
    }

    .volunteer-photo {
      height: 120px;
    }
  }
</style>

<picture>
  <source srcset="/images/pravconf2025-logo.avif" type="image/avif" />
  <source srcset="/images/pravconf2025-logo.webp" type="image/webp" />
  <img src="/images/pravconf2025-logo.png" alt="PravConf 2025 logo, showing a map of Kerala and the text PravConf 2025 on top of a modified version of the Prav logo" width="400" height="400" />
</picture>

We're delighted to announce the first edition of PravConf!

**When?** <time datetime="2025-03-01">1st & 2nd March, 2025</time>\
**Where?** [Model Engineering College (MEC), Kochi](geo:10.02799,76.32843?z=18 "Govt. Model Engineering College Thrikkakara") (view in [OsmApp](https://osmapp.org/dijurmyw), [Organic Maps](https://omaps.app/4xbwwHw7Ms/Govt._Model_Engineering_College), or [OsmAnd](https://osmand.net/map?pin=10.02799,76.32843#18/10.02799/76.32843))

**We are offering free tickets to the first 10 people who join our [public group](xmpp:pravconf@conference.prav.app?join) and contact one of the admins of the group using the Prav App.**

<a class="button" href="https://rzp.io/rzp/CpcdItN">Register</a>
<a class="button"  href="https://app.formbricks.com/s/cm6ep36kj0001l503wjuslc2m" title="PravConf 2025 CFP">Propose a talk</a>

<figure>
<p align="center">
<a title="Sivahari, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Model_Engineering_College_Kochi_Top_view_02.JPG"><img width="512" alt="Model Engineering College Kochi Top view 02" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Model_Engineering_College_Kochi_Top_view_02.JPG/512px-Model_Engineering_College_Kochi_Top_view_02.JPG?20130802081727"></a>
</p>
<figcaption>Model Engineering College, Kochi.</figcaption>
</figure>

# Schedule

| **Date**   | **Time**            | **Session Description**                                  |
|------------|---------------------|---------------------------------------------------------|
| **March 1**| 09:00 to 10:00      | On site registrations                                    |
|            | 10:10 to 10:30      | Opening session                                         |
|            | 10:40 to 11:25      | Introduction to Prav by Pirate Praveen, Ravi, Badri    |
|            | 11:40 to 12:40      | A story of a community hosting by Bady and Kannan       |
|            | 12:40 to 13:50      | Lunch                                                   |
|            | 14:00 to 14:20      | A short introduction to the Fediverse by Badri           |
|            | 14:30 to 15:00      | Email: a federated protocol by Varun                     |
|            | 15:10 to 15:30      | Convo: bringing federated messaging to feature phones    |
|            | 15:40 to 16:30      | Your ideas to promote Prav                               |
| **March 2**| 10:00 onwards       | Session on translating Prav into various languages. People can collaborate on projects they like.  |
|            | 10:00 onwards       | Review and signing of Prav bye-laws                      |
|            | Any time            | Working on random Prav or XMPP related stuff (set up your own server?)|

# FAQ

## What is PravConf 2025?

PravConf 2025 is the first edition of PravConf, an annual gathering of the Prav community.

## Who is the Prav community?

We are users, developers and volunteers in the Prav cooperative aiming to free users from vendor lock-in and invasion of privacy. See the [homepage](/) for more details.

<!-- hide the other questions in a "spoiler" -->
<details>

<summary>More questions</summary>

## I am not part of the community. Can I attend?

Sure! We invite the wider Free Software community interested in privacy, self/community hosting, federation/decentralization to join this meeting to share experiences, and plan campaigns and actions together.

## How do I register?

Press the register button and pay a registration fee of ₹200 per person.

## Why do I have to pay to register?

The registration fee will be used to pay for food for the attendees. The remaining amount will be allocated towards Prav fund. If you'd like to sponsor PravConf 2025, please see [Can I volunteer to help?](#can-i-volunteer-to-help)

## I can't afford ₹200 for the tickets. What should I do?

We are offering free tickets to the first 10 people who register using Prav App, join our [public group](https://xmpp.link/#prav@chat.disroot.org?join) and contact one of the admins of the group.

## Can I volunteer to help?

Yes, please! If you'd like to sponsor or help us organize the event, please join the [publicity and outreach group on XMPP](https://xmpp.link/#prav-publicity-and-outreach@conference.diasp.in%3Fjoin "Prav Publicity and Outreach"), or contact us via [email](mailto:contact@prav.app).

If you'd like to give a talk, fill the [CFP form](https://app.formbricks.com/s/cm6ep36kj0001l503wjuslc2m "PravConf 2025 CFP") and we will contact you.

## How do I reach the venue?

The nearest metro station from the venue is [Edapally](https://omaps.app/0xbwwDR2LW/Edappally). Major railway stations namely, Ernakulam Junction, Ernakulam Town, Tripunithura and Aluva are connected via the Kochi metro network. From Edapally metro station, you can take a bus from the stop on the side of Saravanna Bhavan and deboard at Thrikkakara Amabalam stop. The venue, MEC, is approximately 500 metres from there.

## I have a question not listed here

You can contact us through [publicity and outreach group on XMPP](https://xmpp.link/#prav-publicity-and-outreach@conference.diasp.in%3Fjoin "Prav Publicity and Outreach") or via [email](mailto:contact@prav.app)
</details>

# Sponsors

## Platinum

[![GoodBytes](/images/GoodBytes-Logo.svg)](https://www.goodbytes.nl)

## Gold

[![Mostly Harmless](/images/mostly-harmless.png)](https://mostlyharmless.io)

## Silver

## Bronze

## Supporter

[![FOSS United Foundation](/images/fossunited.svg)](https://fossunited.org)

## Fiscal Hosts

[![Navodaya Networks](/images/navodaya.svg)](https://www.navodaya.world)

[![XMPP Foundation](/images/xmpp-logo-black-text-only.svg)](https://xmpp.org)

## Organising Partner

[![FOSS MEC](/images/fossmec.svg)](https://fossmec.netlify.app)

# Become a sponsor

Check the following table for our sponsorship tiers.

<div style="overflow-x:auto;">

| Benefits 				| Platinum |  Gold | Silver |  Bronze | Supporter 	|
|----------				|----------|----------|----------|----------|----------	|
| Contribution in INR 			| 30k      | 20k      | 15k      | 10k      | 6k       	|
| Logo on webpage    			|  ✓       | ✓        | ✓        | ✓        | ✓         |
| Logo on delegate kit  		|  ✓       | ✓        | ✓        | ✓        | ✓         |
| Logo on 10 min presentation   	|  ✓       | ✓        | ✓        | ✓        |          	|  
| Logo on Stage Backdrop    		|  ✓       | ✓        | ✓        |          |          	|  
| Complimentary tickets  		|  ✓       | ✓        |         |          |          	|   
| Logo on promotional materials  	|  ✓       | ✓        |         |          |          	|   
| Standees/Banner 			|  ✓       | ✓        |         |          |          	|   
| Mention in video releases   		|  ✓       | ✓        |         |          |          	|   
| Distribution of Brochures/Flyers 	|  ✓       |         |         |          |          	|    
| Stall					|  ✓       |         |         |          |          	|    		
| Promotion in pre-event Session 	|  ✓       |         |         |          |          	|    
| Logo on speaker’s kit			|  ✓       |         |         |          |          	|    
| Product promotion in upcoming events 	|  ✓       |         |         |          |          	|    

</div>

For more details, check out our [sponsorship brochure](https://codeberg.org/prav/prav-assets/src/branch/main/PravConf/2025/sponsorship-brochure.pdf). [Contact us](mailto:contact@prav.app) if you'd like to sponsor PravConf 2025.

## Volunteers

<div class="volunteers-list">
     <div
    class="volunteer-card"
    onclick="window.location='https:\/\/social.masto.host\/@praveen';"    
  >
    <img src="/images/badri.jpg" alt="Badri Sunderarajan" class="volunteer-photo" />
    <h3 class="volunteer-name">Badri Sunderarajan</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
    onclick="window.location='https:\/\/merveilles.town\/@untrusem';"
  >
    <img src="/images/moksh.jpg" alt="moksh" class="volunteer-photo" />
    <h3 class="volunteer-name">moksh</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
  >
    <img src="/images/gn.jpeg" alt="Nagarjuna" class="volunteer-photo" />
    <h3 class="volunteer-name">Nagarjuna</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
    onclick="window.location='https:\/\/fe.disroot.org\/@libreinator';"
  >
    <img src="/images/perry.webp" alt="perry" class="volunteer-photo" />
    <h3 class="volunteer-name">perry</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
    onclick="window.location='https:\/\/social.masto.host\/@praveen';"
  >
    <img src="/images/praveen.jpeg" alt="Pirate Praveen" class="volunteer-photo" />
    <h3 class="volunteer-name">Pirate Praveen</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
    onclick="window.location='https:\/\/ravidwivedi.in';"
  >
    <img src="/images/ravi.jpg" alt="Ravi Dwivedi" class="volunteer-photo" />
    <h3 class="volunteer-name">Ravi Dwivedi</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
  
  <div
    class="volunteer-card"
  >
    <img src="" alt="saliaku" class="volunteer-photo" />
    <h3 class="volunteer-name">saliaku</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card"
    onclick="window.location='https:\/\/x.com\/theVRN21';">
    <img src="/images/varun.jpg" alt="Varun" class="volunteer-photo" />
    <h3 class="volunteer-name">Varun</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/roshin.jpeg" alt="Roshin" class="volunteer-photo" />
    <h3 class="volunteer-name">Roshin</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/nikhil.jpeg" alt="Nikhil" class="volunteer-photo" />
    <h3 class="volunteer-name">Nikhil</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/khushi.jpeg" alt="Khushi" class="volunteer-photo" />
    <h3 class="volunteer-name">Khushi</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/jeswin.jpeg" alt="Jeswin" class="volunteer-photo" />
    <h3 class="volunteer-name">Jeswin</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/aadithya.png" alt="Aadithya" class="volunteer-photo" />
    <h3 class="volunteer-name">Aadithya</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/sreepriya.jpg" alt="Sreepriya" class="volunteer-photo" />
    <h3 class="volunteer-name">Sreepriya</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/yasir.jpg" alt="Yasir" class="volunteer-photo" />
    <h3 class="volunteer-name">Yasir</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>

  <div
    class="volunteer-card">
    <img src="/images/nikesh.jpg" alt="Nikesh" class="volunteer-photo" />
    <h3 class="volunteer-name">Nikesh</h3>
    <p class="volunteer-title">Volunteer</p>
  </div>
   
</div>
