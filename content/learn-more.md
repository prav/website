---
title: 'Learn more'
date: 2018-02-22T17:01:34+07:00
draft: false
---
<figure style="float:left;margin:0px 50px 50px 0px">
  <a href="/images/prav-a4-poster.png">
    <img src="/images/prav-a4-poster.png"  width="300">
  </a>
  <figcaption>
    Picture: Flyer explaining ideas behind the Prav project.
    <br>
    By Badri for Prav, CC-BY-SA 4.0
  </figcaption>
</figure>

## What is Prav?

### A messaging app

Prav users can communicate with other Prav users, as well as with users of other apps on the same network. Prav has support for messaging, file sharing, voice messages, audio and video calls.

### No ads. No AI.

Period.

### Easy onboarding

Signing up on Prav is as easy as entering your phone number and entering an OTP received by you by SMS. So you can easily bring your friends and family to Prav. We took the idea from [Quicksy app](https://quicksy.im) which provides convenient sign up using phone number without vendor lock-in (explained below).

### No vendor lock-in

Most popular messaging apps require users to download the same app and have account on the same service. Prav is part of the [XMPP](https://xmpp.org/) network with many other XMPP services, which means you can switch service providers and still talk to your contacts. For example, SIM cards are interoperable. If you switch from Airtel to Vi, you will still be able to make calls with your contacts. Similar thing is not possible with popular apps like WhatsApp, Telegram, Signal etc. If you uninstall the app, you lose all your contacts.

Check [Frequently Asked Questions](/faq) about Prav for more details.

### Social Contract

We shall adhere to our Social Contract:

1. We will put rights of users above profit.

2. Our products will always be committed to user privacy.

3. Our apps and services will always respect users' freedom (100% Free Software app and service).

4. Our service will be interoperable using a free standard (federated with any [XMPP](https://xmpp.org/about/technology-overview/) service). No one will be forced to use our app or service to talk to users of our service. Our users can switch to any other service provider without losing the ability to talk to their existing contacts (similar to sim cards provided by telecom operators).

5. We will donate, whenever possible, to the projects we rely on.

We also have a Code of Conduct (CoC) which you can find [here](/coc)

### Software Freedom

Prav is [Free Software](https://www.gnu.org/philosophy/free-software-even-more-important.html). Users have the freedom to run, study, modify, share and share the modified versions.

## Meet our team

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/badri.jpg"  style="height:250px;">
</figure>

### Badri Sunderarajan

*Helped in setting up initial ejabberd server, domain and hosting, as well as designing posters for Prav Private Beta Release.*

Hello! My name is Badri. I am a self-taught programmer and interested in all things FOSS! I try to avoid corporate platforms wherever possible, and encourage others to do the same. I am also an avid reader of books, as well as a writer of mainly nonfiction. E-paper is my screen of choice.

I'm a co-founding editor of the [Snipette magazine](https://www.snipettemag.com/), where I also help to run our YunoHost server which runs services like Ghost and Mattermost and some [custom-developed tools](https://code.snipettemag.com/snipette). I also work part-time for the [Planet Hool foundation](https://planethool.org/), where I convinced them to use XMPP as the basis for their card-game app. Other contributions include the [Sawaliram Project](https://sawaliram.org/), and some internal organisational scripts for the Chennai Mathematical Institute (CMI).

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/george-jose.jpg" style="height:250px;">
</figure>

### George Jose

Hi George here. I am a full stack web developer (LAMP, MEVN, MERN). I initially got hooked onto Prav since I was tired of the existing messengers flaws. I was missing from the discussions for some time due to my work schedule, I hope I can contribute something for the community.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/mujeebcpy.jpg" style="height:250px;">
</figure>

### Mujeebcpy

*Helped Prav project grow by making a YouTube video in Malayalam.*

Hi, My name is Mujeeb Rahman. Me and my friends run a web development company ([Alpha fork](http://alphafork.com/)) with the help of FOSS tools. we also part of migration of a news paper organization to completely move to foss. including scribus and kubuntu. i run a youtube channel named as [IBcomputing](http://youtube.com/ibcomputing). I use this channel to promote FOSS and privacy related topics and general tech topics.
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/gn.jpeg" style="height:250px;">
</figure>

### Nagarjuna

Hi, I am Nagarjuna (GN), a STEM educationist with roots in biology and philosophy. Seeking and facilitating freedom, truth, and justice in diverse, decentralized, and distributed structures in codified and real life. Former Professor at Gnowledge lab at HBCSE, TIFR.
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/nikhil.png" style="height:250px;">
</figure>

### Nikhil

Hello, My name is Nikhil, I am an economics student at IIFT Delhi.

I also do sysadmin stuff, i like to tweak system, software, hardware.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/praveen.jpeg"  style="height:250px;">
</figure>

### [Pirate Praveen](https://social.masto.host/@praveen)

*First member of Prav project. Helped in outreach and supervising the project.Also, guided Ravi for deploying Quicksy server.*

A skilled labourer in a social emergency campaigning to remake this world as he sees fit (a pirate). For a world where everyone have enough to survive and can do what they really love to do as opposed to what pays them the most.

> “The acquisition of wealth is no longer the driving force in our lives. We work to better ourselves and the rest of humanity.” — Jean Luc Picard, Star Trek First Contact.

A Debian Developer, privacy advocate and a politician. Mentors students to contribute to Free Software.
<br>
<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/pratik.jpg"  style="width:200px;">
</figure>
<br>

### Pratik M.
*Helped in maintaining Prav website.*

Namaste! Pratik here 🙂. I'm a smile freak( though not smiling in the pic:), a high school student with many dreams, and of course, a Free Software enthusiast. I don't have technical wisdom, so, I'm trying to help write some docs(and probably Bangla translation), so that, when Prav becomes a hit, I can smile and say to myself that I had a tiny part there 😉

<br>
<br>
<br>
<br>
<br>
<br>

<figure style="float:left;margin:0px 50px 50px 0px">
<img src="/images/ravi.jpg"  style="height:250px;">
</figure>

### [Ravi Dwivedi](https://ravidwivedi.in)
*website maintenance, social media, server setup, and project outreach*
<br>
<br>
A [Free Software](https://www.gnu.org/philosophy/free-sw.html) activist and privacy advocate.

An active member of [the Free Software Community of India](https://fsci.in) and the Indian Debian community. Started [the Indian LibreOffice community](https://blog.documentfoundation.org/blog/2022/03/19/join-the-indian-libreoffice-community/).

He holds an M.Math from Indian Statistical Institute (Kolkata), and B.Sc. Mathematics from Acharya Narendra Dev College, Delhi.</p>

He writes at [ravidwivedi.in](https://ravidwivedi.in) and toots on [toot.io/@ravi](https://toot.io/@ravi).
