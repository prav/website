---
title: "Homepage"
meta_title: "Prav"
description: "Prav Messaging App - Privacy and convenience with choice of providers"
intro_image: "/images/prav-screenshot.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# Prav - Private Messenger

Prav is a **messaging service** which can be used to exchange messages, audio/video calls, files, images and videos over the Internet.

Inspired by the [Quicksy](https://quicksy.im/) app, Prav provides the **convenience** of registering with a phone number.

<a href="https://f-droid.org/en/packages/app.prav.client"><img src="/images/f-droid.svg" width="200"></img></a> 
<a href="https://play.google.com/store/apps/details?id=app.prav.client"><img src="/images/google-play.svg" width="200"></img></a> 

Popular messaging apps only allow you to talk to users using the same app. However, Prav allows you to talk to all the users on the same network even if they use other apps like [Quicksy](https://quicksy.im/), [Monocles Chat](https://monocles.chat), [Dino](https://dino.im/), [Gajim](https://gajim.org/), [Monal](https://monal-im.org/), and many more. In other words, Prav has **no [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in)**.

Lastly, Prav is a **cooperative** (in the process of registration) which allows anyone to become a member and vote on decisions, such as the privacy policy or what features should be added.

<p>
  <a href="/learn-more" class="button">Learn More</a>
  <a href="/get-involved" class="button">Get Involved</a>
</p>

We are currently in public beta, and raising funds for a sysadmin to provide production-grade service. To help with this, you can either [donate directly](https://prav.app/donate/) or pre-order your Prav account and get discounts.

<p>
  <a href="/pre-order" class="button">Pre-order your account</a>
</p>

As of 12th of February 2025, Prav has **330** registered users.
