---
title: "Announcing first edition of PravConf 2025"
date: 2025-01-30T00:54:39+05:30
draft: false
author: false
---
We’re delighted to announce the first edition of [PravConf](https://prav.app/conf/2025/)! The conference will be held on the 1st and 2nd March 2025 at [Model Engineering College (MEC), Kochi](geo:10.02799,76.32843?z=18 "Govt. Model Engineering College Thrikkakara"). You are invited to join us. Registrations and talk submissions are open. Please check the [conference webpage](https://prav.app/conf/2025/) for the same. You can help us by [becoming a sponsor](https://codeberg.org/prav/prav-assets/src/branch/main/PravConf/2025/sponsorship-brochure.pdf). 

In order to get updates, please join our [announcements mailing list](https://lists.fsci.in/postorius/lists/announcements.prav.app/) or follow us on [Fediverse](https://venera.social/profile/prav).
