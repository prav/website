---
title: "Preparing to register a Multi-State Cooperative"
date: 2024-11-25T14:04:00+05:30
draft: false
author: false
---

Until now, Prav has been operating on an informal basis. This was a good way to start, as we could get things moving without being held back by formalities and paperwork.

However, we are now reaching a point where registration is necessary. Some examples of this:

- Since we don't have our own bank account, we are currently receiving donations via [the Navodaya Networks Forum](https://navodaya.world) and XMPP Foundation. This creates an additional expense-tracking workload on our side as well as theirs.
- International transactions are somewhat hit-and-miss from India. Every time we want to make payments for online resources like our domain or server, we have to find a volunteer with an international card to make the payment, and then reimburse the amount to them.
- We hired Softrate to implement custom username support in Prav. As part of this process, we had to sign a contract, but since there was no authorised signatory, it was signed by a random member of the community. In future, this might not be advisable.
- For publishing on app stores like Google Play and Apple's App store, one has to either register as an individual or provide a DUNS number. Obtaining that DUNS number is possible only if we are formally registered in some way.

Because of all these speedbumps, we have decided to start the process of formally registering Prav as a multi-state cooperative society.

# The challenge

A multi-state cooperative is not as common as other options like a partnership or a nonprofit company. This means there is less documentation available on how the process works. Additionally, we have to collect signatures and authorisations from all the dozens of members, in order to register.

Once we have initiated this process, we will also have to start the formal operations of a cooperative such as electing a board of directors and holding regular meetings—again, there are questions to be figured out such as whether the voting for such elections has to be in person or whether it can be done remotely through procedures like postal ballot.

We tried reaching out to some lawyers specialising in this process who wouldbe willing to handle all of the registration, but for a high fee. At this point, it did not make sense to go for that without at least attempting to do it on our own.

# What about easier options?

Why we settled on a cooperative model is a topic for a different article, but besides providing "easy onboarding for standards-based messaging", experimenting with this cooperative models was one of the main ideas behind Prav. Briefly, it is an attempt to explore cooperative societies as a way to address some of the problems with more "mainstream alternative" models such as an informal grouping or a standard NGO.

We could have gone with an easier option for now, such as forming a normal partership and switching to the cooperative model later. This would unblock other parts of Prav operations, such as being able to make payments directly. However, it would also take significant effort even to register that simpler organisation, and once done there would be less motivation to do that effort all over again to switch to a cooperative. Since our goal is to eventually form a cooperative anyway, there's no time like the present to get this done.

What we're attempting is not going to be the easiest path. However, once we accomplish it, we can share what we learn and pave the way for more such initiatives to spring up in future. Like the messenger pigeon of yore, we hope the Prav pigeon can also send a message to future initiatives: another model is possible.

# How do we manage until then?

Until we have our own legal entity going, we are managing without formal registration thanks to the generous support of [Navodaya Networks Forum](https://www.navodaya.world/) and [the XMPP Standards Foundation](https://xmpp.org/) which accepts donations on our behalf.

We are also thankful to individuals who directly sponsor some expenses like cost of server hosting, SMS gateway, domain name renewals or printing leaflets and posters at events. We are also grateful to those who authorise their credit or debit cards to make the actual transactions, on our behalf, for the online services we use.

# Call to action

A cooperative society is formed by people coming together, and Prav is no different. Here are some areas where you can join in the togetherness to make this cooperative a success (as well as, hopefully, help other cooperatives in the future!)

- **If you like writing and editing:** help draft updates for the blog and social media—either in English or in your local language. (Could we have phrased this list better? Let us know!)
- **If you are good at reading long texts and figuring out regulations:** go through some of the relevant legal documents and help figure out what the implications are for humans. Having a law background is a bonus, but as we couldn't find any professional lawyers who were free to help, anyone else with a knack for rule-wrangling is welcome. (Wink to board gamers!)
- **If you're not scared of picking up the phone:** help us reach out to the 100+ people in Kerala and Maharashtra who have signed up to be members of the cooperative. We need to collect all their signatures as well as collect membership fees in order to submit our application to the government. Bonus if you know Marathi or Malayalam.
- **If you like spreadsheets and checklists:** join in to help manage the whole operation, and make sure things happen when they're supposed to.
- **If you want to be involved in Prav, long-term:** [register to be a cooperative member yourself](/become-a-member/)! We currently have the numbers only in Kerala and Maharashtra, but if you're from a different state, we can aim to register there as well once we cross 50 members.

To get involved with any of this you can do any of the following:
- join our group chat (compatible with Prav, Quicksy, and other XMPP apps): <xmpp:prav-coop@conference.diasp.in?join>
- send us an email: <mailto:contact@prav.app>
- learn more on the website: <https://prav.app/volunteer>
- if someone from Prav sent you to this post, you can ask them directly to put you in touch :)
What next?
Registering a multi-state cooperative society is an invoved process with many moving parts that have to be fit together in the right order. The details are something we may never fully know until we have tried it, but we've made some progress in figuring things out.

Some of this is documented on [the Codema thread](https://codema.in/d/rFYWu1UR/coop-registration-process-status-update-and-call-for-volunteers) and we will soon be putting out a blog post outlining everything that we know so far.

Let the bureaucracy begin! ✊🏼