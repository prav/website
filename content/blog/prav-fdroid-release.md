---
title: "Prav is now available from F-Droid store for Free Software Andoid apps"
date: 2023-10-26T20:59:55+05:30
draft: false
author: false
---

Good news! Prav is now available to download from the F-Droid app store,
a repository of Free Software apps we recommend for Android users. If you do
not already have F-Droid, you can get it from https://f-droid.org.

Once you install F-Droid, you can just search for "prav" and install it like many
other Free Software apps already available from F-Droid. This will also notify
you whenever a new version of Prav is available.

<img src="/images/prav-fdroid.jpg" alt="Prav on F-Droid" style="height:400px;">

This also means Prav has gone through a strict review and validation for its
Free Software credentials. Every time we make a new release, F-Droid build servers
will build a new Android package (APK) and publish on their store. This is another
milestone for us in making Prav available to everyone.
