---
title: "Why register a cooperative?"
date: 2024-11-26T13:10:00+05:30
draft: false
author: false
---

<div style="text-align: center;">

![A pigeon looking up at a cow](/images/coop-register/pigeon-and-cow.png)

</div>

Today, the 26th of November, is recognised as National Milk Day, in honour of Verghese Kurien who made [Amul](https://amul.com) a success. In 1946, the private dairy Polson had a monopoly on milk, and was squeezing the farmers of Kaira (now Khera) in Gujarat with its low pay.

Amul was formed in response to this situation. Led by [Tribhuvandas Patel](https://en.wikipedia.org/wiki/Tribhuvandas_Kishibhai_Patel), the farmers of Kaira got together to form a cooperative society so they could sell milk on their own terms. This cooperative eventually evolved into Amul, which under the management of Verghese Kurien became a huge success and turned Amul into a model that was replicated by milk cooperatives across the country.

Prav does not sell milk.

However, Prav is involved in something that we think has a comparable impact on today's life and society: sending messages to people.

Since we have started on the road to formally register as a cooperative, we thought this was a good time to look back at why Prav chose this model and what its implications are.

# What is a cooperative society?

Unlike companies which tend to be controlled by a handful of people, cooperatives are usually a coming-together of entire communities.

In technical terms, regular companies (including nonprofits) have a board of directors elected by shareholders, based on how many shares they have. That means people with more money can (by buying more shares) have an outside influence on the direction the company takes. By contrast, cooperatives are operated on the principle of "one person, one vote"—so your vote counts the same whether you have one share or a thousand.

Rather than solely seeking profits, cooperatives tend to have a mission to help society in some way (which is true for nonprofits as well). This mission can be given a legal backing by incorporating it into the cooperative's bye laws—so if any member, or even the entire board of directors, starts to lead it astray, the members can take steps to prevent it.

Some large cooperatives operating in India include:

- Amul and other state milk federations, which are usually owned by milk producers although the state government often has a stake as well.
- The various cooperatives behind the Indian Coffee House restaurant chain. This initiative came up in 1936 in response to "Coffee Houses" run by British colonisers, into which native Indians were not allowed to enter.
- HOPCOMS, the Horticultural Producers' Cooperative Marketing and Processing Society which enables direct marketing of farm products in Karnataka.
- Various cooperative banks, housing societies, and groups in specific industries like sugar or khadi are also often run as cooperatives.

While most of the examples listed above are of very large cooperatives, there are lakhs of others that run on a much smaller or more local scale. This includes Shreyas, which was formed to ensure essential commodities like rice and pencils were available at affordable prices in Ahmedabad, and the Uralungal Labour Contract Cooperative Society (ULCCS) in north Kerala that consists exclusively of construction workers and labourers and was the first such organisation in India.

# Tech on the service lane

What we have not seen so far is a cooperative centred around information technology and the digital world.

One could say technology in India is at a crossroads when it comes to being self-sufficient versus depending on outside entities. Much of our technological infrastructure is dependent on a handful of usually foreign companies. One only has to list Meta (Facebook, WhatsApp, Instagram), Google (Gmail, YouTube, Google Pay), and Microsoft (GitHub, Outlook, LinkedIn) to cover most of it.

Take email, which is designed for self-ownership. At what are meant to be the premier technological institutions of India, none of the IITs founded in the 1990s or later run their own email server. Instead, they rely on Google, or, in a few cases, Microsoft. (To their credit, the IITs of Kharagpur, Bombay, Madras, Kanpur, and Delhi still run their own email servers, although newer programmes like the IIT Madras Online Degree are again dependent on Google).

Given this situation, perhaps one cannot say we are at a crossroads, but rather, on the service lane with a finite opportunity to get back onto the highway.

This dependency on a few large entities is not good for multiple reasons. For one, it encourages monopolistic practices, as we are left with no choice but to follow what the company dictates. When WhatsApp enacted its privacy-invasive data-sharing practices with Facebook in 2021, users were not happy—but they had no choice but to go along with it, The related case resurfaced in the Supreme Court only last month, with Meta aggregating peoples' data for the three interim years and continuing to do so.

Since most of these tech companies are based abroad, this is also a geopolitical vulnerability. One only has to look at the Venezuelan designers who were unable to access their Adobe accounts and work—even though they had paid for it—due to sudden sanctions from the USA.

# Where Prav comes in

The tech companies we depend on are well equipped with large teams and billions of dollars of funding. Can we really match up to their scale? The answer is, maybe we don't have to.

Those who sell us digital products would have us believe that running our own tech services is difficult. In reality, it has been and continues to be done on a small scale by various groups, organisations, and enthusiastic individuals around the world. We believe the answer to a handful of big tech companies is not a handful of bigger, techier, companies, but numerous small, grassroots-run initiatives that bend technology to serve their local needs.

Prav is one such initiative.

Like other initiatives, Prav had something to trigger it. What those triggers were is the kind of thing that will seem obvious in retrospect while being exceedingly hard to determine in the present, but perhaps they would go something like this:

- The fact that messaging platforms started running closed services, and closing off their previously open services, preventing people to exchange messages with users of other messaging platforms.
- The fact that this vendor lock-in made people effectively trapped with the largest service providers, even though those providers may not necessarily have been acting in the best interests of their users.
- The desire to address these issues on a larger scale, rather than only at a personal or small-group level.

# Why not some other kind of legal entity?

Initiatives like this are often structured in a more centralised way, with a few people at the top authorised to make decisions. This works because the leadership would listen to the community anyway, even if they are not legally obligated to do so.

However, when things go wrong—let's say the board of directors want to do something that the majority of the community is opposed to—there's no way to stop them. Similarly, if a controversial issue comes up that divides the community, the leaders could have a tendency to go with their own views instead of coming to a proper compromise.

A registered cooperative would work much the same as any other organisation (or informal grouping) when things are going fine. But when things are not fine, a registered cooperative would make accountability to members legally enforceable. Even during good times, cooperatives increase accountability by requiring financial reports to be submitted every year (unlike when operations are informally managed).

The other difference of a cooperation is that it has elected leaders. In the case of Prav, the number of leadership terms are limited, ensuring that different people get a chance at the helm. Members will be able to elect leaders, and replace any leadership that does not match their expectations. On the other side, the leadership would have to work hard to retain the confidence of the members, rather than the members becoming dependent on the leadership.

In the longer term, many initatives that are centred around a few people tend to collapse when those people leave, lose interest, or get busy with other things. By having a 'bottom up' instead of a 'top down' approach, we hope to make Prav into an initiative that sustains itself without being dependent on a few specific people to keep it going.

# The cost of democracy

In a nutshell, the cooperative approach is a democratic approach. Like all democracies, though, it is on the members to remain ever vigilant and avoid voting in the wrong kind of leader or making decisions that are against our own interests.

Besides having our guiding principles represented in the cooperative bye-laws, this means constantly reminding ourselves of what those principles are—because if we don't, someone may convince us to vote and change those bye-laws themselves.

What if the bye-laws do need changes? That is the reason we have the provision to change them if enough people agree, rather than having it set in stone as in a trust (another model we could have gone with). Hopefully, enough of the members at that time will be aware enough of all the principles and consequences in order to make the right decision.

Operating in this way also means being careful about whom we onboard into the cooperative. We need people who feel a part of the process and take ownership, not passive observers who could be swayed either way by what somebody tells them.

Registering a cooperative will not automatically solve everything. One only has to look at the numerous cooperative banks, that have been taken over by local politicians and are not necessarily the best managed, to realise that. But like all choices, this will open up new options even as it closes off others—and we're excited to discover, together, what those new options are.