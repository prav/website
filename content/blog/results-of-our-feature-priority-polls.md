---
title: "Top priority features for Prav"
date: 2023-04-07T02:39:52+05:30
draft: false
author: false
---
After having a private beta launch, we did a poll with people who showed interest in planning the project, to prioritize development of features in Prav app. The results of the poll are out. We will focus our resources on these features as priority now.

Below is the screenshot of the outcome of the poll:

<figure>
<img src="/images/priority-feature-poll.png">
<figcaption>Screenshot of results of our poll</figcaption>
</figure>

Some of the features are useful for the whole xmpp protocol, while others are goals specific to Prav project.

The features ranked in order of priority is as follows:

1. Fix key sharing issues in encrypted groups (with commonly used XMPP clients).

2. Support message deletion/moderation.

3. Standardized SMS otp based sign up and login (using SASL2/fast so any XMPP app can be used with Prav account).

4. Develop Prav for iOS.

5. Support automated backups and restore on demand.

6. Web based registration for Prav account.

7. Prav for GNU/Linux (operating systems like Ubuntu, Debian, Arch, fedora, etc.)

8. Reply to messages in Prav.

9. Feature to mark some messages as important to find them later.

10. Emojis and Stickers support.

11. Develop Prav for MacOS.

12. Develop Prav for Windows.

13. Provide an option to switch to Prav as default SMS app.

14. Add Custom theme option (including chat background).

Note that you can use emojis and stickers in Prav even now if your keyboard supports it.

We decide on our group on [codema](https://codema.in). If you are interested in planning the project, you can [contact us](#contact).
