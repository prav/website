---
title: "Prav is now in Public Beta!"
date: 2024-09-08T12:00:00+05:30
draft: false
summary: Anyone is now welcome to install Prav from F-Droid
---

For the past 22 months, we have been testing Prav privately among the community. Today, we are happy to announce that we are in public beta.

The Prav app is now available on [on F-Droid](https://f-droid.org/en/packages/app.prav.client/) for anyone to download and use for free. We hope to soon make it available on the Play Store and App Store as well.

While the Prav app is usable, issues that crop up during production may take some time to get fixed. In order to run a production-level service, we would need a sysadmin on call to quickly come and troubleshoot things. If you would like to volunteer for this role, please [contact us](https://prav.app/volunteer/).

You can also help fund a sysadmin, either by [pre-ordering your Prav account](https://prav.app/pre-order/) or through [a direct donation](https://prav.app/donate/).

# Changes since our Private Beta release

- SMS delivery was configured, so people can sign up through an OTP on their phone number. [^1]
- HTTP upload and image sharing were set up
- Message retraction was implemented in Prav
- Message moderation was enabled for groups - group admins can delete messages posted by other members (this helps to prevent spam)
- Prav was made available for download [on F-Droid](https://f-droid.org/en/packages/app.prav.client/)

[^1]: During Private Beta, SMS delively wasn't working so we had to manually set and report OTPs.
