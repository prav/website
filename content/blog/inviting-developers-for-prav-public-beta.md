---
title: "Inviting Developers for Prav Public Beta Release"
date: 2023-06-14T18:52:55+05:30
draft: false
author: false
---
Prav is currently in private beta and we are planning to release a public beta soon for everyone to try out the service. We are looking for a Java developer with knowledge of Android app development. Please send us your proposals along with the amount of money required for you to work on the project.

## Features to implement

We need these features to be implemented in the public beta version of the Prav app:

1. Users should be able to sign up with a custom username instead of taking phone number as username. This will need change in both app and server side.

2. Publish the app in [F-Droid](https://f-droid.org) store.

## Suggested order of steps

1. Modify Prav server to allow creating custom username (this will add the xmpp id and phone number mapping in the directory instead of using phone number as username)

2. Modify login screen to add an option to choose a custom username.

	1. Add a warning popup about using phone number as username and offer an option to choose custom username

		a. In direct chats and private groups, people will see your phone number and in public groups, admins will see your phone number. If you want to chat with someone new, you will have to share your phone number.

		b. If you use xmpp to irc bridges or join rooms which use such bridges, your phone number will be public.

	2. Use the newly created API to create account with custom username and link to phone number in the directory.

## Contact us to apply

If you are interested, please use the contact details in the [footer](#contact) of this page to reach us. The source code of the Prav app is [here](https://codeberg.org/prav/prav). We are tracking the progress of public beta release [here](https://codeberg.org/prav/IssueTracker/milestone/4008).
