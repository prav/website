---
title: "Now you can get a Prav account by simply entering an OTP received by SMS on your phone"
date: 2023-09-12T18:37:52+05:30
draft: false
author: false
---
Good news: we are one step closer to the Prav public beta release!

Earlier we had to create an account manually on the server and people
had to explicitly request us an account by filling a form on the
website. This is not required anymore :)

Prav now supports SMS registration via OTP. Thanks to Jishnu,
Siddharth, Arya, Kiran and Akshay. We were unable to send SMS via OTP
earlier because the Quicksy Server only supported the older deprecated
version of Twilio (PravServer is based on Quicksy Server) and we had to
update Prav Server to work with the newer version of twilio API.  We sat
together at debconf 23 in Kochi and [fixed this](https://codeberg.org/prav/IssueTracker/issues/4). Thanks to debconf 23
for this opportunity.

You can download the app from https://beta.prav.app. Once installed,
enter your phone number in the app and get an OTP to register an account.

However, remember that the app is still in private beta. This means it
is subject to changes and may need more bug fixes. We still don't
recommend using Prav as your primary communication medium but we hope to
be able to reach there sometime soon.

In the next step, we hope to upload on F-Droid app store which will make
it easy for you to install the app. Going forward, we want to upload the
app on Google Play Store and Apple app store too.

Feel free to test and report issues if any or get in [touch with us](https://prav.app/#contact).
