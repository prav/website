---
title: "Crowdfunding for Custom Username support"
date: 2024-10-08T18:21:00+05:30
draft: false
author: false
---

One of the priority features that were voted on for Prav is custom username support. This was supposed to be implemented before our Public Beta launch, but was delayed as we did not find anyone to work on it. We are now in touch with a company, Softrate India, who has agreed to take this on, but we need your help raising funds amounting to 23,000 INR (~300 USD).

The Softrate team has already started working on this, so if we meet our goals we will hopefully see custom username support come to Prav very soon!

You can donate through the following buttons, or [visit the fundraising page](/donate/custom-username/) for more details.

<script src="https://opencollective.com/custom-username/donate/button.js" color="blue"></script>

<div class="razorpay-embed-btn pulse" data-url="https://pages.razorpay.com/pl_NseNHjx2s9qd00/view?reason=Custom+Username+Support" data-text="Donate Now!" data-color="#528FF0" data-size="large" style="text-align: center;">
  <script>
    (function(){
      var d=document; var x=!d.getElementById('razorpay-embed-btn-js')
      if(x){ var s=d.createElement('script'); s.defer=!0;s.id='razorpay-embed-btn-js';
      s.src='https://cdn.razorpay.com/static/embed_btn/bundle.js';d.body.appendChild(s);} else{var rzp=window['__rzp__'];
      rzp && rzp.init && rzp.init()}})();
  </script>
</div>

<p class="text-center"><a class="button" href="/donate/custom-username/">Visit the fundraising page</a></p>
