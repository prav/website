---
title: "Prav Initial Version is ready"
date: 2022-12-07
draft: false
author: false
---
Good news!

We have initial version of Prav ready and working. We will test the app with people who are interested in the project and looking to have a public beta release soon.

Many thanks to all who helped us reach this milestone. Please check the [credits page](/credits) for more details.
