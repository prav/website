---
title: "Nagarathna Memorial Grant sponsors Prav"
date: 2024-07-29T15:50:00+05:30
draft: false
summary: "We are delighted to share that Nagarathna Memorial Grant is now a sponser of Prav Project. We are very grateful to Thejesh GN for the grant."
---
![Nagarathna Memorial Grant Logo](/images/nagarathna-memorial-grant-logo.png)

It is with immense pleasure that we announce the Prav project [has been awarded](https://thejeshgn.com/2024/04/12/nmg-2024-results/) a lump sum of ₹40,000 from the Nagarathna Memorial Grant. This amount is to go towards System Administration, Website Maintenace, and UX Design. The grant application was filed by Ravi, Vinay, Nilabrata, and Arya.

The [Nagarathna Memorial Grant](https://thejeshgn.com/projects/nagarathna-memorial-grant/) is an annual microgrant by [Thejesh GN](https://thejeshgn.com/about/) for projects with a "meaningful objective".

The aim of Prav is to build a messaging ecosystem which is independent, easy to use, and cooperative. Our social contract ensures that we would never abuse our users for profit. We at Prav are thankful for the support from the Nagarathna Memorial Grant. It will help to ensure that the we, the Prav team, can continue our fight against the monopoly of WhatsApp.
