---
title: "Custom username support"
summary: "Allow people to set a custom username for their Prav account instead of using their phone number (which is the default)"
date: 2024-10-08T18:21:00+05:30
target: 23000
reached: 23022
suggested_amount:
  usd: 20
active: true
donation_pages:
  opencollective: "custom-username"
  razorpay: "Custom Username Support"
---

One of the priority features that were voted on for Prav is custom username support. This was supposed to be implemented before our Public Beta launch, but was delayed as we did not find anyone to work on it. We are now in touch with a company, Softrate India, who has agreed to take this on, but we need your help raising funds amounting to 23,000 INR (~300 USD).

The Softrate team has already started working on this, so if we meet our goals we will hopefully see custom username support come to Prav very soon!

# What is this "custom username" we are supporting?

Currently, Prav addresses are of the format `phonenumber@prav.app`. That means anyone who has your contact in XMPP will also be able to deduce your phone number. Our planned custom username feature in Prav app will allow users to optionally choose a username instead of a phone number, creating an XMPP address of the form `username@prav.app`.

Contact discovery using phone address book will still work after choosing a username using the XMPP id <-> phone number mapping directory feature of the Prav server.

The Quicksy directory already allows linking any XMPP id to a phone number and, being based on the Quicksy server, the Prav server can also utilise the same feature. Using this feature, people will be able to discover someone's XMPP id if they have their phone number, but not the other way round.

# Why is this important?

By default, your phone number becomes your username, but this means it gets exposed publicly in contexts you might not want:

- In private groupchats, if JIDs are public, everyone in the group chat will be able to get your phone number
- In public groupchats where admins are allowed to see everyone's JID, they will effectively be seeing your phone number too
- When connecting to IRC via a biboumi bridge, your username (and therefore phone number) gets exposed
- When connecting to a matrix room via bifrost bridge portal rooms, you phone number gets exposed (plumbed rooms don't have this problem)
- If you want to text someone who doesn't already have your phone number, you'll end up sharing your phone number with them
- Even if your full JID is not exposed, you have to set a nickname globally or on a per-group basis. Some clients default to using the beginning of your XMPP id as a nickname—which in this case will end up being your phone number

We are seeing similar problems being addressed by other messaging services too. Telegram gives the option to set a username instead of sharing your phone number with contacts; Signal also recently added this feature.

# If it's so important, why are we keeping it optional?

While setting a custom username is a privacy boost, it adds an extra step to the signup process. People who are not likely to join public groups or don't mind their phone number exposed may still prefer less steps to get started.

A greater potential problem is that people are used to connecting to each other by sharing their phone number. If they set a custom username, they can still connect to other Prav users through their phone number thanks to contact discovery. However, users of other XMPP apps and services will require a username in order to get connected. So Prav users who opt for this would have to get used to sharing their full XMPP id rather than their phone number.

Since this is a new and crucial change, we feel it's important to test this in the field before making it the default. Depending on how people end up using to this option, and based on their feedback, we can revisit this decision later (and possibly make this the default).

