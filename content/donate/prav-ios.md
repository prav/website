---
title: "Prav on iOS"
summary: "This fundraiser is to build an iOS version of the Prav app based on Monal, to make Prav available to iPhone users"
date: 2024-01-08T20:59:55+05:30
target: 600
reached: 553.49
units:
  code: USD
  name: dollars
  symbol: "$"
active: true
donation_pages:
  opencollective: "prav-ios"
  razorpay: "Prav For iOS"
---

# Background
Historically, XMPP support on Apple iOS has been lacking compared to Android. Many things have improved in recent times (for example end-to-end encryption and audio video calls in Monal), but there are still some areas that needs work. One of the biggest missing pieces is lack of a Quicksy-like application on iOS which makes XMPP onboarding very easy on Android. Quicksy allows sign up using phone numbers and sending OTP to authenticate, similar to WhatsApp, Telegram, Signal. This approach has been successful to onboard newbies compared to other XMPP apps.

[A video message from Kannan VM in Malayalam.](https://videos.fsci.in/videos/embed/1a9d26aa-f078-4346-8d8a-be167c74fc37)

**Update:** On January 3rd 2024, [Monal developers confirmed they are working on Quicksy like functionality in Monal](https://github.com/monal-im/Monal/issues/565#issuecomment-1875148650).

# Current Status
Currently the recommended options for iOS are,
1. Monal IM which like other XMPP apps need a username and password.
2. Snikket is easier if you have a Snikket invite or you have to create account from a provider that allow sign up using a web browser. 

This makes sign up complicated for newbies who are used to phone number based sign ups.

Additionally, unlike WhatsApp, Telegram or Signal you don't discover your existing contacts already on XMPP.

# Other XMPP apps

Siskin is another option which still have some issues (for example defaults to suggesting servers that need email confirmation, but this is not conveyed to the user, making users unable to login or chat).

# What is the plan?

This project aims to build Quicksy API server support for sign ups so users can sign up using an SMS OTP on the Prav service (this could be reused by Quicksy if they are interested) and discover other Prav users based the phone addressbook. This will be a fork of Snikket iOS or Monal and maintained as Prav app for iOS by Prav project. A decision on the base app will be made through a poll in the Prav community. See https://prav.app/blog/xmpp-apps-for-ios/ for a detailed comparison of available options.

**Update:** Since Monal is working on Quicksy like functionality, we will base our work on that to build Prav iOS. We will need funds for this rebuild, publishing on Apple Store and building custom username support (unlike Quicksy, we want to allow people to choose a custom username instead of their phone number as username).

# Who will get the money?

The funds raised through this will be given to a developer(s) who implements this. Currently Arya K (who maintains Prav on Android) has agreed to work on this project. We will also need money to publish apps on Apple Store (apple developer account costs 99 USD per year).

# What is Prav?

Prav is similar to Quicksy in functionality, but is in the process of registering a cooperative society in India so users democratically decide the privacy policy and prioritize feature development (Prav on iOS was prioritized through a recent poll among Prav contributors, this came a close second to fixing new device trust for end to end encryption).

Learn More about Prav at https://prav.app Credit: Prav is built on https://Quicksy.im

You can also donate directly to Monal https://monal-im.org/support/#donate

# Why are you building Prav on iOS?

We often get this question, "Why we are building Prav on iOS since Apple is evil?", from many  Free Software supporters. Unlike Libre Office or Debian, a personal choice is insufficient to beat WhatsApp due to network effects.

Even people who start using Prav on Android will be forced to go back to WhatsApp even if a single contact is on iOS and they find it hard to join XMPP without the easy on-boarding option provided by Quicksy and Prav (this has already happened to many of us who tried to on-board people to Quicksy).

Non availability of Prav won't be enough reason for people to leave iOS altogether.

# Other payment options

If you are unable to make a payment via this page or prefer payment via Indian Bank cards or UPI, then you can donate using https://pages.razorpay.com/prav-app

# References

1.  Discussion with Monal developers https://github.com/monal-im/Monal/issues/565
