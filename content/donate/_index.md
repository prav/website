---
title: "Donate to Prav"
date: 2024-04-04T14:11:40+05:30
draft: false
author: false
---

At Prav, we do not have any corporate sponsors. We expect to get funded by our users buying subscriptions after the service is launched and through our shareholders investing initial amount. Donations can help us go extra mile as it gives people who believe in our philosophy to support  us without getting involved or downloading our app.

You can donate directly to the Prav app by using the following buttons, or scroll down to see the full campaign list.

  <a class="button" href="https://opencollective.com/pravapp">Fund via Open Collective</a>
  <a class="button" href="https://pages.razorpay.com/pl_NseNHjx2s9qd00/view">Fund via Razorpay</a>

***Note:** Open Collective supports all global payment methods. Razorpay supports India-specific methods like UPI and is preferred for donors based in India*
