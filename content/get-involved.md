---
title: 'Get involved'
date: 2024-06-02T14:08:26+0530
draft: false
---

Are you interested in supporting our mission? Or are you not yet sure but just want to lurk on the sidelines? Whatever your situation, here's what you can do to get involved!

# Stay up to date

To get updates about Prav, you can join our announcements mailing list via email.

For those who like real-time discussions, you are also welcome to join our XMPP chat room. However, we may not always post official updates there, so we recommend you sign up for email updates as well.

<a class="button"  href="https://lists.fsci.in/postorius/lists/announcements.prav.app/">Sign up for email updates</a>
<a class="button" href="https://join.jabber.network/#prav@chat.disroot.org?join">Join the group chat</a>

# Contribute to the Prav project

We are looking for volunteers on various tasks—including development, but also artwork, writing, publicity, and design.

If you have funds to spare, we are also running campaigns to raise funds for some of these tasks. Donating to these campaigns will help us pay volunteers to do to tasks we need.

<a class="button" href="/volunteer">Contribute your time</a>
<a class="button" href="/donate">Contribute your money</a>

# Become a beta tester or join our cooperative

Prav is now in public beta, which means you are welcome to download the app and share your feedback on it. Our app is already available on the F-droid free software app store, and we are working it on publishing it to Google's Play Store as well.

If you are interested in taking part in the decision-making process of Prav, you can register to become a member of our (to be registered) cooperative society.


<a class="button" href="https://f-droid.org/en/packages/app.prav.client/">Try out the app</a>
<a class="button" href="/become-a-member">Become A Member</a>

---

If you have any other queries or feedback, you can get in touch via email at **prav at fsci dot in**
