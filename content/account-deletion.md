---
title: "Account Deletion"
---
## Account Deletion

To delete your account, please e-mail your details to [contact@prav.app](mailto:contact@prav.app). We will contact you for verification, and then proceed to deleting your account.

## Deleting account from the app

You can delete your Prav account from the app itself. From main screen => open the overflow menu => Manage Account => open overflow menu again => Delete account. You can delete your account from the server too, by opting it with the checkbox provided.

## Steps
 - <img src="/images/deletion_one.png" height="250">
 - <img src="/images/deletion_two.png" height="250">
 - <img src="/images/deletion_three.png" height="250">