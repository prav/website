---
title: PravConf
---

PravConf is an annual gathering of the Prav community. We invite the users and developers of Prav, and the wider free software community interested in privacy, self/community hosting and the fediverse to join us to share your experience, plan campaigns and actions together.

# PravConf 2025

PravConf 2025, the first edition, is planned to be at MEC Kochi. Visit the [PravConf 2025 page](2025 "PravConf 2025") for more information.
