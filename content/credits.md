---
title: "Credits"
date: 2025-02-10T10:57:41+05:30
draft: false
author: false
---
## Contributors

- Pirate Praveen: Helped in outreach and supervising the project and guided Ravi for deploying Quicksy server. Prepared draft byelaws for registering as a cooperative based on CFCICI byelaws and conducted polls to review changes. Brought many people to the community as coop members or contributed. Helped with promotions through social media and through events.

- Chagai for building the first Prav APK.

- Ravi Dwivedi helped in maintaining Prav website, social handle, setting up prav server and project outreach activities.

- Akshay S Dinesh created CSV verification provider and helped by troubleshooting from time to time and adapting quicksy server.

- Badri and Suman did the initial ejabberd setup.

- Sajith and Badri helped in domain and hosting.

 - Nagarjuna brought many people to the community as coop members and helped manage finances via Navodaya.

- Raghukamath designed the logo.

- Mani built the beta release APK.

- [VGLUG](https://vglug.org) hosted Prav beta release party and translated the poster in Tamil.

- Badri created the beta release party poster and the countdown for beta release on the website.

- Jishnu, Siddharth, Arya, Kiran and Akshay worked on OTP integration.

- Arya updated the andoid apk build with retraction feature and published on [F-Droid](https://f-droid.org).

- Vishnu Sanal T helped publish Prav App on Google Play Store. [Here](https://vishnusanal.github.io/posts/publishing-prav-on-play-store/) is a blog on it.

- Joice sponsored Google developer account for Prav.

- Buster helped with drafting Prav Co-Operative Bye-Laws and Vonage SMS provider subscription.

- perry helped make the PravConf 2025 page on the website.

- [Moksh](https://merveilles.town/@untrusem) helped with sponsorship brochure and website.

## Artwork

See [artwork page](/artwork)

## Software we forked

- [Quicksy](https://quicksy.im) client and Quicksy server.

## Software we use

- [Debian](https://debian.org)

- ejabberd

- [hugo](https://gohugo.io/)

- Hugo Serif Theme by www.zerostatic.io for the website

- [Penpot](https://penpot.app) for designing posters.

## Services we use

- [FSCI mailing lists](https://lists.fsci.in)

- [codeberg](https://codeberg.org)

- [kobotoolbox](https://kobotoolbox.org)

- [Cryptpad](https://cryptpad.fr)

- [codema](https://codema.in)

- [formbricks](https://formbricks.com)
